workflow fq_count {

    File fastq_file
	String container = "ubuntu@sha256:ba394fabd516b39ccf8597ec656a9ddd7d0a2688ed8cb373ca7ac9b6fe67848f"

    call count_seqs { input: infile = fastq_file, container = container }
}

task count_seqs {
    File infile
	String container

    parameter_meta {
      infile: "A description of what the fastq should be and what used for."
      mo: "A description of what the fastq should be and what used for."
    } 

    command <<<
        wc -l ${infile} > num_seqs.txt
        #echo image: $SHIFTER_IMAGEREQUEST
    >>>

    output {
        File outfile = "num_seqs.txt"
    }

    runtime {
	  time: "00:20:00"
	  memory: "1G"
	  cpu:  2
	  maxRetries: 1
	  docker: container
    }
}

